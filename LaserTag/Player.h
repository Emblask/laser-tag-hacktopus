#ifndef PLAYER_H
#define PLAYER_H

#include <Arduino.h>
#include <inttypes.h>

#include "DispManager.h"
#include "GameParam.h"
#include "Pinout.h"
#include "PlayerList.h"
  
class Player{
  public:
    GameMode game_mode;
    int8_t team;
    int8_t id;
    int8_t hp;
    
    DispManager* disp_manager;
    
  public:
    Player();
    Player(DispManager* disp_manager, GameMode gameMode, int8_t team, int8_t id);
    void respawn(); 
    void stun(int8_t idEnemy);
    void hit(int8_t idEnemy);
    void shoot();
};

#endif

#ifndef GAME_PARAM_H
#define GAME_PARAM_H

//Game constantes
#define STUN_TIME 3 //secondes
#define HP_MAX 20
#define DAMAGE 20
#define N_TEAMS 3

enum GameMode {
  TDM,
  DM
};


#endif

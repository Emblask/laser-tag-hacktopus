#ifndef PLAYER_LIST_H
#define PLAYER_LIST_H

#include <Arduino.h>
#include <inttypes.h>

//Players
const int8_t INVI = 0;
const int8_t YOYO = 1;
const int8_t JOJO = 2;
const int8_t SEB = 3;
const int8_t BILL = 4;
const int8_t EVA = 5;
const int8_t GALY = 6;
const int8_t RAPH = 7;
const int8_t BEN = 8;

const int8_t N_PLAYERS = 9;
//! THE ORDER OF NAMES IS IMPORTANT
const int8_t PLAYERS [] =
{
  INVI,
  YOYO,
  JOJO,
  SEB,
  BILL,
  EVA,
  GALY,
  RAPH,
  BEN,
};

#endif

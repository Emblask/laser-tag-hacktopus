#ifndef DISP_MANAGER_H
#define DISP_MANAGER_H

#include <Arduino.h>
#include <inttypes.h>

#include "Alphabet.h"
#include "Displayer.h"
#include "GameParam.h"
#include "PlayerList.h"


class DispManager
{
  public:
    Displayer displayer;

    DispManager();
    void disp_status(GameMode game_mode, int8_t team, int8_t id, int8_t hp);
    void disp_game_mode(GameMode game_mode);
    void disp_team(int8_t team);
    void disp_player(int8_t id);
    void disp_score(int8_t score);
    
    void disp_dead();
    void disp_end();
    
    static void trad_team(int8_t team, int8_t SegData[]);
    static void trad_id(int8_t spec, int8_t SegData[]);
};

#endif

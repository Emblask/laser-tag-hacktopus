#ifndef LASER_TAG_H
#define LASER_TAG_H

#include "Pinout.h"
#include "GameManager.h"

//Game
GameManager game_manager;

//Reception
bool validFrame = false;  // whether the string is complete
bool flag_shoot = false;
int8_t idPlayer = 0;
int8_t idTeam = 0;

// Functions prototype
void setup();// Base function initialising everything
void loop(); // Main infinite loop waiting for IR

void setIrModOutput(); // Electronic shit happening there
void serial_init(); // Start serial port
void serialEvent();// at the end of loop() get info on serial port
void shoot_ISR(); // raise a flag when shooting
void select_ISR();
void ok_ISR();

#endif

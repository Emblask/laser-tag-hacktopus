#include "Player.h"

Player::Player()
{
  
}

Player::Player(DispManager* disp_manager, GameMode gameMode, int8_t team, int8_t id)
{
  this->disp_manager = disp_manager;
  this->game_mode = game_mode;
  this->team = team;
  this->id = id;
  respawn();
}

void Player::respawn()
{
  this->hp = HP_MAX;
  digitalWrite(aliveLed, HIGH);
}


//Routine attente quand HIT (paralysie)
void Player::stun(int8_t idEnemy) {
  //Delay
  disp_manager->disp_player(idEnemy);
  digitalWrite(aliveLed, LOW);
  delay(STUN_TIME*1000);
  digitalWrite(aliveLed, HIGH);
  disp_manager->disp_status(game_mode, team, id, hp);
}

void Player::hit(int8_t idEnemy){
  //Perte de HP
  hp -= DAMAGE;
  
  //Si plus de HP
  if (hp <=0){

    disp_manager->disp_dead();
    delay(1000);
    
    stun(idEnemy);
    respawn();
    disp_manager->disp_status(game_mode, team,id, hp);
  } else {
    disp_manager->disp_status(game_mode, team, id, hp);
    stun(idEnemy);
  }
  
  //Un flush manuel de toutes les trames reçues durant un état dead ou hit
  while (Serial.available()) {
    Serial.read();
  }
}

void Player::shoot(){
  Serial.print((char)FIRST_BYTE_FRAME);
   /*idByte: 7 bits
  => 5 bits LSB GAMER_TAG de 0 à 31
  => 2 bits MSB GAMER_TEAM 0, 1, 2 ou 3*/
  int8_t idByteToSend = (((team & MASK_TEAM_WRITE) << SHIFT_TEAM) | (id & MASK_ID));
  Serial.print((char)idByteToSend); //idByte 7 bits: 5 LSB idPersonne et 2 MSB idEquipe 
  Serial.print((char)idByteToSend); //pas utilisé pour le moment
  Serial.print((char)LAST_BYTE_FRAME);
}

#include "LaserTag.h"

void setup() {

  //UART
  serial_init();
  
  //INPUT
  pinMode(selectButton, INPUT);
  pinMode(okButton, INPUT);
  pinMode(shootButton, INPUT);
  attachInterrupt(digitalPinToInterrupt(shootButton), shoot_ISR, RISING);
  
  //OUTPUT
  //pinMode(beep, OUTPUT);
  pinMode(aliveLed, OUTPUT);
  pinMode(shootLed, OUTPUT);
  setIrModOutput(); //LED Shoot
  TIMSK2 = _BV(OCIE2B); // Output Compare Match B Interrupt Enable

  //Variables
  validFrame = false;
  flag_shoot = false;
  
  //Game  
  game_manager = GameManager();
  game_manager.restart();
}

void loop() {
  //Reception
  if (validFrame) {
    validFrame = false;
    game_manager.input_hit(idTeam, idPlayer);
  }

  if(game_manager.game_state == PLAYING)
  {
    if(flag_shoot)
    {
      game_manager.input_shoot();
      flag_shoot = false;
    }
      
    if(digitalRead(selectButton) == HIGH)
      game_manager.end_menu();
  }
}

//Interruptions
void shoot_ISR(){
  flag_shoot = true;
}

void serial_init(){
  Serial.begin(2400); // 2400 bps
  while(!Serial){;}
}

void setIrModOutput(){  // sets pin 3 going at the IR modulation rate
  TCCR2A = _BV(COM2B1) | _BV(WGM21) | _BV(WGM20); // Just enable output on Pin 3 and disable it on Pin 11
  TCCR2B = _BV(WGM22) | _BV(CS22);
  OCR2A = 35; // defines the frequency 51 = 38.4 KHz, 54 = 36.2 KHz, 58 = 34 KHz, 62 = 32 KHz
  OCR2B = 17;  // deines the duty cycle - Half the OCR2A value for 50%
  TCCR2B = TCCR2B & 0b00111000 | 0x2; // select a prescale value of 8:1 of the system clock
}


/*
  Fonction appelée automatiquement à la fin de chaque loop pour recevoir les données sur la photo diodes.
  Types de message:
  Coup reçu
  ...
*/
void serialEvent() {
  while (Serial.available()) {
    int8_t timeout = 0;
    // get the new byte:
    int8_t firstReceivedByte = Serial.read(); //Premier byte reçu correspondant au byte de démarrage de la trame
    // Vérifier l'intégrité de ce message:
    if(firstReceivedByte == FIRST_BYTE_FRAME)
    {
      while ((Serial.available() < 3) && (timeout < 100))
      {
        delayMicroseconds(1000); // wait 1ms
        timeout++;
      }
      if(timeout < 100)
      {
        //1er  octet de données
        int8_t idByte = Serial.read();
        idPlayer = idByte & MASK_ID;
        idTeam = (idByte & MASK_TEAM_READ) >> SHIFT_TEAM;
        
        int8_t byteSupp = Serial.read(); 
        //TODO: A utiliser pour d'autres fonctionnalités par la suite
        //Actuellement on utilise byteSupp pour comparer au idByte et vérifier son intégrité
        //Indiquer la fin de la trame et donc l'intégrité de la trame 
        int8_t lastReceivedByte = Serial.read();
        if((lastReceivedByte == LAST_BYTE_FRAME) && (idByte == byteSupp))
        {
          validFrame = true;
        }  
      } 
      else
      {
        //Timeout: désynchronisation ou mauvaise trame
      }
    }
  }
}

//Shoot module : interuption, setup PWM et fonction de tir
ISR(TIMER2_COMPB_vect){  // Interrupt service routine to pulse the modulated pin 3

}

#ifndef ALPHABET_H
#define ALPHABET_H

#define xA 0x77
#define xB 0x7c
#define xC 0x39
#define xD 0x5e
#define xE 0x79
#define xF 0x71
#define xG 0x3d
#define xH 0x76
#define xI 0x10
#define xJ 0x1e
#define xL 0x38
#define xM 0x37
#define xN 0x54
#define xO 0x5c
#define xP 0x73
#define xQ 0x67
#define xS 0x5d
#define xT 0x70
#define xU 0x3e
#define xV 0x1c
#define xY 0x6e
#define xZ 0x5b
#define xNULL 0x00


#define x0 0x3F
#define x1 0x06
#define x2 0x5b
#define x3 0x4f
#define x4 0x66
#define x5 0x6d
#define x6 0x7d
#define x7 0x07
#define x8 0x7f
#define x9 0x6f

#endif

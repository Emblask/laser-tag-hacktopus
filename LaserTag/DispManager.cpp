#include "DispManager.h"

DispManager::DispManager()
{
  displayer = Displayer();
}

void DispManager::disp_status(GameMode game_mode, int8_t team, int8_t id, int8_t hp)
{
  int8_t first = (game_mode == DM) ? id : team;
  int8_t msg[4] = {
    displayer.trad_number(first), 
    xNULL, 
    displayer.trad_number((hp/10)%10),
    displayer.trad_number(hp%10)
  };
  
  displayer.disp(msg);
}

void DispManager::disp_score(int8_t score)
{
  int8_t msg[4] = {
    displayer.trad_number((score/1000)%10), 
    displayer.trad_number((score/100)%10), 
    displayer.trad_number((score/10)%10),
    displayer.trad_number(score%10)
  };
  
  displayer.disp(msg);
}

void DispManager::disp_dead()
{
  int8_t msg[4] = {xD, xE, xA, xD};
  displayer.disp(msg);
}

void DispManager::disp_end()
{
  int8_t msg[4] = {xNULL, xE, xN, xD};
  displayer.disp(msg);
}

void DispManager::disp_game_mode(GameMode game_mode)
{
  switch(game_mode){
    case DM:
      int8_t msg_dm[4] = {xD, xM, xNULL, xNULL};
      displayer.disp(msg_dm);
      break;
    case TDM:
      int8_t msg_tdm[4] = {xT, xD, xM, xNULL};
      displayer.disp(msg_tdm);
      break;
    default:
      int8_t msg_error[4] = {xNULL, xNULL, xNULL, xNULL};
      displayer.disp(msg_error);
      break;
  }
}

void DispManager::disp_team(int8_t team)
{
  int8_t msg[4];
  trad_team(team, msg);
  
  displayer.disp(msg);
}
//void DispManager::trad_team(int8_t team, int8_t SegData[]){
//  switch(team){
//    case 0:
//    SegData[0] = xO;
//    SegData[1] = xU;
//    SegData[2] = xP;
//    SegData[3] = xS;
//    break;
//    case 1:
//    SegData[0] = xP;
//    SegData[1] = xI;
//    SegData[2] = xF;
//    SegData[3] = xNULL;
//    break;
//    case 2:
//    SegData[0] = xP;
//    SegData[1] = xA;
//    SegData[2] = xF;
//    SegData[3] = xNULL;
//    break;
//    case 3:
//    SegData[0] = xP;
//    SegData[1] = xO;
//    SegData[2] = xU;
//    SegData[3] = xF;
//    break;
//  }
//}

void DispManager::trad_team(int8_t team, int8_t SegData[]){
  switch(team){
    case 0:
    SegData[0] = xE;
    SegData[1] = x0;
    SegData[2] = xNULL;
    SegData[3] = xNULL;
    break;
    case 1:
    SegData[0] = xE;
    SegData[1] = x1;
    SegData[2] = xNULL;
    SegData[3] = xNULL;
    break;
    case 2:
    SegData[0] = xE;
    SegData[1] = x2;
    SegData[2] = xNULL;
    SegData[3] = xNULL;
    break;
    case 3:
    SegData[0] = xE;
    SegData[1] = x3;
    SegData[2] = xNULL;
    SegData[3] = xNULL;
    break;
  }
}

void DispManager::disp_player(int8_t id)
{
  int8_t msg[4];
  trad_id(id, msg);
  
  displayer.disp(msg);
}

void DispManager::trad_id(int8_t spec, int8_t SegData[]){
  switch(spec){
    case INVI:
    SegData[0] = xI;
    SegData[1] = xN;
    SegData[2] = xV;
    SegData[3] = xI;
    break;
    case YOYO:
    SegData[0] = xY;
    SegData[1] = xO;
    SegData[2] = xY;
    SegData[3] = xO;
    break;
    case JOJO:
    SegData[0] = xJ;
    SegData[1] = xO;
    SegData[2] = xJ;
    SegData[3] = xO;
    break;
    case SEB:
    SegData[0] = xS;
    SegData[1] = xE;
    SegData[2] = xB;
    SegData[3] = xNULL;
    break;
    case BILL:
    SegData[0] = xB;
    SegData[1] = xI;
    SegData[2] = xL;
    SegData[3] = xL;
    break;
    case EVA:
    SegData[0] = xE;
    SegData[1] = xV;
    SegData[2] = xA;
    SegData[3] = xNULL;
    break;
    case GALY:
    SegData[0] = xG;
    SegData[1] = xA;
    SegData[2] = xL;
    SegData[3] = xY;
    break;
    case RAPH:
    SegData[0] = xP;
    SegData[1] = xA;
    SegData[2] = xP;
    SegData[3] = xH;
    break;
    case BEN:
    SegData[0] = xB;
    SegData[1] = xE;
    SegData[2] = xN;
    SegData[3] = xNULL;
    break;
    default:
    SegData[0] = xNULL;
    SegData[1] = xNULL;
    SegData[2] = xNULL;
    SegData[3] = xNULL;
    break;
  }
}

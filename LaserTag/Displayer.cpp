#include "Displayer.h"

Displayer::Displayer(){
  
  int8_t text_tmp[] = {0x00,0x00,0x00,0x00};
  set_text(text_tmp);

  tm1637 = new TM1637(disp_CLK,disp_DIO);
  
  tm1637->set(7);   //0 éclairage faible réglable jusqu'à 7 le plus intense
  tm1637->init();   //Initialisation du driver 
  tm1637->point(false); //: off
}

void Displayer::set_text(int8_t output_text[]){

  for(int8_t i = 0; i < 4 ;++i)
  {
    text[i] = output_text[i];
  }
}


void Displayer::disp(){

  tm1637->start();          //start signal sent to TM1637 from MCU
  tm1637->writeByte(ADDR_AUTO);//
  tm1637->stop();           //
  tm1637->start();          //
  tm1637->writeByte(tm1637->Cmd_SetAddr);//
  for(int i=0;i < 4;i ++){
    tm1637->writeByte(text[i]);        //
  }
  tm1637->stop();           //
  tm1637->start();          //
  tm1637->writeByte(tm1637->Cmd_DispCtrl);//
  tm1637->stop();   

}

void Displayer::disp(int8_t output_text[]){
  
  set_text(output_text);
  disp();
}

int8_t Displayer::trad_number(int8_t number)
{
  int8_t result;
  switch(number)
  {
    case 0:
      result = x0;
      break;
    case 1:
      result = x1;
      break;
    case 2:
      result = x2;
      break;
    case 3:
      result = x3;
      break;
    case 4:
      result = x4;
      break;
    case 5:
      result = x5;
      break;
    case 6:
      result = x6;
      break;
    case 7:
      result = x7;
      break;
    case 8:
      result = x8;
      break;
    case 9:
      result = x9;
      break;
    default:
      result = xNULL;
      break;
  }

  return result;
}

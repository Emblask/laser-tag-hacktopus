#ifndef GAME_MANAGER_H
#define GAME_MANAGER_H

#include <Arduino.h>
#include <inttypes.h>

#include "DispManager.h"
#include "GameParam.h"
#include "Player.h"

enum GameState{
  PARAM,
  PLAYING,
  END
};

class GameManager
{
  public:
    GameState game_state;
    GameMode game_mode;
    Player player;
    DispManager disp_manager;
    
    int8_t score[N_PLAYERS];
    
    GameManager();
    void (* reset_arduino) (void) = 0;
    void restart();
    void end_menu();
    void execute();
    void input_shoot();
    void input_hit(int8_t id_team, int8_t id_enemy);
    
  private:
    void init_score();
    void get_worst_enemy(int8_t* id_enemy, int8_t* max_score);
    
    void param_game();
    void start_game();
    void end_game();
    
    GameMode select_mode();
    int8_t select_team();
    int8_t select_player();
};

#endif

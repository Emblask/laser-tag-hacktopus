#ifndef PINOUT_H
#define PINOUT_H

#include <Arduino.h>
#include <inttypes.h>

//Digital
const int8_t shootButton = 2;   
const int8_t selectButton = 4;
const int8_t okButton = 7;     
const int8_t aliveLed = 9;      
const int8_t shootLed = 3;
//Analog
//const int8_t beep = A1;
const int8_t disp_CLK = A5;
const int8_t disp_DIO = A4;

//Serial
const int8_t FIRST_BYTE_FRAME = 65; //"A"
const int8_t LAST_BYTE_FRAME = 66; //"B"
const int8_t MASK_TEAM_WRITE = 0b11;
const int8_t MASK_TEAM_READ = 0b1100000; //0x60
const int8_t SHIFT_TEAM = 5;
const int8_t MASK_ID = 0b11111; //0x1F

#endif

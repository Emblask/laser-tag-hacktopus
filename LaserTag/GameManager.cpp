#include "GameManager.h"

GameManager::GameManager()
{
  game_state = PARAM;
  disp_manager = DispManager();
}

void GameManager::restart()
{
  init_score();
  param_game();
  start_game();
}

void GameManager::execute()
{
  //TODO? Create a function that will be called from arduino loop to operate
  //Instead of having while functions
}

void GameManager::init_score()
{
  for(int8_t i = 0; i < N_PLAYERS; ++i)
  {
    score[i] = 0;
  }
}

void GameManager::get_worst_enemy(int8_t* id_enemy, int8_t* max_score)
{
  id_enemy = 0;
  max_score = 0;
  for(int8_t i = 0; i < N_PLAYERS; ++i)
  {
    if(max_score < score[i])
    {
      max_score = score[i];
      id_enemy = PLAYERS[i];
    }
  }
}

void GameManager::param_game()
{
  GameMode game_mode = select_mode();
  int8_t team = 0;
  if(game_mode == TDM)
    team = select_team();
  int8_t id = select_player();

  player = Player(&disp_manager, game_mode, team, id);
  disp_manager.disp_status(player.game_mode, player.team, player.id, player.hp);
}

void GameManager::start_game()
{
  game_state = PLAYING;
}

void GameManager::end_game()
{
  game_state = END;
  disp_manager.disp_end();
  bool disp_tag_score = true;
  int8_t worst_enemy_id = 0;
  int8_t worst_enemy_score = 0;
  
  get_worst_enemy(&worst_enemy_id, &worst_enemy_score);
  disp_manager.disp_player(worst_enemy_id);

  while(digitalRead(selectButton) == HIGH || digitalRead(okButton) == HIGH);
  while(digitalRead(okButton) == LOW){
    if(digitalRead(selectButton) == HIGH)
    {
      disp_tag_score != disp_tag_score;

      if(disp_tag_score)
        disp_manager.disp_player(worst_enemy_id);
      else
        disp_manager.disp_player(worst_enemy_score);
        
      while(digitalRead(okButton) == HIGH);
    }
  };
  reset_arduino();
  //restart();
}

void GameManager::end_menu()
{
  disp_manager.disp_end();

  while(digitalRead(selectButton) == HIGH || digitalRead(okButton) == HIGH);
  while(digitalRead(selectButton) == LOW && digitalRead(okButton) == LOW);

  if(digitalRead(okButton) == HIGH)
  {
    end_game();
  }
}

GameMode GameManager::select_mode()
{
  GameMode mode = DM;
  disp_manager.disp_game_mode(mode);

  while(digitalRead(selectButton) == HIGH || digitalRead(okButton) == HIGH);
  while(digitalRead(okButton) == LOW)
  {
    if(digitalRead(selectButton) == HIGH && mode == DM)
    {
      mode = TDM;
      disp_manager.disp_game_mode(mode);
      while(digitalRead(selectButton) == HIGH);
    }
      
    if(digitalRead(selectButton) == HIGH && mode == TDM)
    {
      mode = DM;
      disp_manager.disp_game_mode(mode);
      while(digitalRead(selectButton) == HIGH);
    } 
  }
  
  return mode;
}

int8_t GameManager::select_team()
{
  int8_t team = 0;
  disp_manager.disp_team(team);
  
  while(digitalRead(selectButton) == HIGH || digitalRead(okButton) == HIGH);
  while(digitalRead(okButton) == LOW)
  {
    if(digitalRead(selectButton) == HIGH)
    {
      team = (team + 1) % N_TEAMS;
      disp_manager.disp_team(team);
      while(digitalRead(selectButton) == HIGH);
    }
  }
  
  return team;
}

int8_t GameManager::select_player()
{
  int8_t player_index = 0;
  int8_t player = PLAYERS[player_index];
  disp_manager.disp_player(player);

  while(digitalRead(selectButton) == HIGH || digitalRead(okButton) == HIGH);
  while(digitalRead(okButton) == LOW)
  {
    if(digitalRead(selectButton) == HIGH)
    {
      
      player_index = (player_index + 1) % N_PLAYERS;
      player = PLAYERS[player_index];
      disp_manager.disp_player(player);
      while(digitalRead(selectButton) == HIGH);
    }
  }
  
  return player;
}

void GameManager::input_shoot()
{
  if(game_state == PLAYING)
    player.shoot();
}


void GameManager::input_hit(int8_t id_team, int8_t id_enemy)
{
  if(game_state == PLAYING && player.id != id_enemy)
    if(player.game_mode == DM || player.team != id_team)
    {
      player.hit(id_enemy);
      ++score[id_enemy];
    }  
}

#ifndef DISPLAYER_H
#define DISPLAYER_H

#include "TM1637.h"  //librairie du driver du 7 segments

#include "Alphabet.h" // Alphabet for player name
#include "Pinout.h"

class Displayer{
private:
  int8_t text[4];
  TM1637* tm1637;        //Pointeur int dans le constructeur tm1637


public:
	Displayer();	
	void set_text(int8_t output_text[]);
	void disp();
	void disp(int8_t output_text[]);
  int8_t trad_number(int8_t number);
};

#endif
